# 任务书数据文件
DefaultQuests.json文件中保存着全局的任务书数据，但在游戏中修改任务书只会变动存档中的任务书数据，编辑完后需要手动后复制到DefaultQuests.json中并提交！

存档中的任务书数据地址：.minecraft\saves\存档名\betterquesting\QuestDatabase.json