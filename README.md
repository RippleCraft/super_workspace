# 超级周目魔改工作区

### 介绍
这里是涟漪超级周目的魔改文件工作区。

### 文件夹结构
1. config文件夹用于存放mod的配置文件，**任务书数据也包含在内，而mods文件夹上传在发行版中**。
2. dakimakura-mod文件夹用于存放自定义抱枕数据。
3. resources文件夹用于存放**cot**添加的材料、方块的本地化文件以及贴图文件等资源文件。
4. scripts文件夹用于存放**crt**魔改的**ZenScript**脚本文件。**如果要参与魔改，为避免冲突，在修改文件之前请先提前告知参与魔改的其他人，最好创建一个分支再最后合并**。

### 魔改参考
#### crt与mt的文档
https://docs.blamejared.com/1.12/zh/

#### gi机器魔改文档
https://github.com/Gregicality/gregicality/wiki/CraftTweaker-Machines

#### 神秘时代6魔改教程-mc百科
https://www.mcmod.cn/post/985.html

#### 末影接口魔改文档
https://github.com/Shadows-of-Fire/EnderTweaker/blob/master/Documentation.md

#### 匠魂魔改教程-mc百科
https://www.mcmod.cn/post/613.html

#### cot文档
https://docs.blamejared.com/1.12/zh/Mods/ContentTweaker/WalkThrough/

#### cot教程-mc百科
https://www.mcmod.cn/post/1463.html

#### 自定义抱枕参考-mc百科
https://www.mcmod.cn/post/695.html