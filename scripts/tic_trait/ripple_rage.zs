#loader contenttweaker
#priority 10
import crafttweaker.item.IItemDefinition;
import crafttweaker.events.IEventManager;
import crafttweaker.event.PlayerAttackEntityEvent;
import crafttweaker.player.IPlayer;
import crafttweaker.item.IItemStack;
import crafttweaker.world.IWorld;
import mods.contenttweaker.tconstruct.TraitBuilder;

//嗜血者
val ripple_rage = mods.contenttweaker.tconstruct.TraitBuilder.create("ripple_rage");
ripple_rage.color = 0xFFD700;
ripple_rage.localizedName = ("嗜血者");
ripple_rage.localizedDescription = ("这把武器的攻击总能让你恢复[造成伤害值*20%]的生命值");
ripple_rage.onHit = function(trait, tool, attacker, target, damage, isCritical) {
    attacker.health = attacker.health+damage*0.2;
};
ripple_rage.register();