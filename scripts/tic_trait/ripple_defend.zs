#loader contenttweaker
#priority 10
import crafttweaker.item.IItemDefinition;
import crafttweaker.events.IEventManager;
import crafttweaker.event.PlayerAttackEntityEvent;
import crafttweaker.player.IPlayer;
import crafttweaker.item.IItemStack;
import crafttweaker.world.IWorld;
import mods.contenttweaker.tconstruct.TraitBuilder;
import crafttweaker.damage.IDamageSource;

//护心
val ripple_defend = mods.contenttweaker.tconstruct.TraitBuilder.create("ripple_defend");
ripple_defend.color = 	0xFFD700;
ripple_defend.localizedName = ("护心");
ripple_defend.localizedDescription = ("-手持该工具时，你受到的伤害不会超过2点");
//添加效果
ripple_defend.onPlayerHurt = function(trait, tool, player,attacker, event) {
    //将event类型转换为crafttweaker.event.EntityLivingHurtEvent
    val new_event = event as crafttweaker.event.EntityLivingHurtEvent;
    //获取伤害
    val damage as float = new_event.amount;
    if(damage > 2){
        var cure as float = damage--2;
        player.health = player.health+cure;
    }
};
ripple_defend.register();