#loader contenttweaker
#priority 10
import crafttweaker.item.IItemDefinition;
import crafttweaker.events.IEventManager;
import crafttweaker.event.PlayerAttackEntityEvent;
import crafttweaker.player.IPlayer;
import crafttweaker.item.IItemStack;
import crafttweaker.world.IWorld;
import mods.contenttweaker.tconstruct.TraitBuilder;

//断钢
val ripple_punishment = mods.contenttweaker.tconstruct.TraitBuilder.create("ripple_punishment");
ripple_punishment.color = 	0xFFD700;
ripple_punishment.localizedName = ("断钢");
ripple_punishment.localizedDescription = ("-你造成的暴击总会使目标当前生命值减半");
//添加效果
ripple_punishment.onHit = function(trait, tool, attacker, target, damage, isCritical) {
    if(isCritical==true){
        target.health = target.health * 0.5;
    }
};
ripple_punishment.register();