//移除融化铱
mods.tconstruct.Melting.removeRecipe(<liquid:iridium>);
//移除融化钛
mods.tconstruct.Melting.removeRecipe(<liquid:titanium>);
//移除融化钯
mods.tconstruct.Melting.removeRecipe(<liquid:palladium>);
//移除虚金匠魂冶炼炉合金配方
mods.tconstruct.Alloy.removeRecipe(<liquid:nihilite_fluid>);
//移除生铁冶炼炉合金配方
mods.tconstruct.Alloy.removeRecipe(<liquid:pig_iron>);
//添加虚金注魔配方
mods.thaumcraft.Infusion.registerRecipe("xujing", "", <taiga:nihilite_ingot>, 80, [<aspect:metallum>*100,<aspect:infernum>*200,<aspect:draco>*30,<aspect:caeles>*20], <thaumadditions:mithminite_ingot>, [<taiga:solarium_ingot>,<taiga:vibranium_ingot>,<thaumcraft:salis_mundus>,<thaumcraft:salis_mundus>,<minecraft:nether_star>,<minecraft:nether_star>]); 