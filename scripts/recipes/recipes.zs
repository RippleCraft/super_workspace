//This file was created via CT-GUI! Editing it is not advised!
//Don't touch me!
//#Remove
furnace.remove(<gregtech:meta_item_1:10074>);
recipes.remove(<portablecraft:portableenderchest>);
recipes.remove(<portablecraft:portablechest>);
recipes.remove(<tconstruct:throwball:1>);
furnace.remove(<gregtech:meta_item_1:10047>);
recipes.remove(<enderio:item_liquid_conduit>);
recipes.remove(<gregtech:machine:2223>);
recipes.remove(<gregtech:machine:2222>);
recipes.remove(<gregtech:machine:2221>);
furnace.remove(<gregtech:meta_item_1:10001>);
recipes.remove(<enderio:item_material:17>);
recipes.remove(<enderio:item_material:19>);
recipes.remove(<enderio:item_material:16>);
recipes.remove(<enderio:item_material:18>);
recipes.remove(<enderio:item_material:15>);
recipes.remove(<enderio:item_material:14>);
recipes.remove(<enderio:item_end_steel_boots>);
recipes.remove(<enderio:item_end_steel_leggings>);
recipes.remove(<enderio:item_end_steel_chestplate>);
recipes.remove(<enderio:item_end_steel_helmet>);
recipes.remove(<enderio:item_dark_steel_boots>);
recipes.remove(<enderio:item_dark_steel_leggings>);
recipes.remove(<enderio:item_dark_steel_chestplate>);
recipes.remove(<enderio:item_dark_steel_helmet>);
recipes.remove(<extrautils2:goldenlasso>);
furnace.remove(<gregtech:meta_item_2:32015>);
recipes.remove(<enderio:item_material:66>);
recipes.remove(<enderio:item_material:55>);
recipes.remove(<enderio:item_basic_capacitor:2>);
recipes.remove(<enderio:item_basic_capacitor:1>);
recipes.remove(<enderio:item_basic_capacitor>);
furnace.remove(<gregtech:meta_item_1:10051>);
recipes.removeShaped(<enderio:block_simple_stirling_generator>);
recipes.remove(<enderio:block_simple_alloy_smelter>);
recipes.removeShaped(<enderio:block_simple_furnace>);
recipes.remove(<enderio:block_simple_sag_mill>);
recipes.remove(<enderio:item_material:0>);
recipes.remove(<gregtech:meta_item_2:32443>);
recipes.remove(<minecraft:flint_and_steel>);
recipes.remove(<minecraft:bucket>);
recipes.remove(<minecraft:iron_boots>);
recipes.remove(<minecraft:iron_leggings>);
recipes.remove(<minecraft:iron_chestplate>);
recipes.remove(<minecraft:iron_helmet>);
//Don't touch me!
//#Add

recipes.addShaped(<enderio:item_liquid_conduit> * 4, [[<gtadditions:ga_meta_item:71>, <enderio:item_material:4>, <gtadditions:ga_meta_item:71>],[<gtadditions:ga_meta_item:71>, <enderio:item_material:4>, <gtadditions:ga_meta_item:71>], [<gtadditions:ga_meta_item:71>, <enderio:item_material:4>, <gtadditions:ga_meta_item:71>]]);
recipes.addShaped(<gregtech:meta_item_2:32454>, [[null, <minecraft:glass>, null],[<minecraft:glass>, null, <minecraft:glass>], [null, null, null]]);
furnace.addRecipe(<gregtech:meta_item_2:32015>, <minecraft:brick>, 0.0);
recipes.addShaped(<enderio:item_material:54>, [[null, <gregtech:cable:969>, null],[<enderio:item_alloy_ingot:8>, <gregtech:machine:504>, <enderio:item_alloy_ingot:8>], [null, <gregtech:cable:969>, null]]);
recipes.addShaped(<enderio:item_material:55>, [[null, <gregtech:cable:5112>, null],[<enderio:item_material:56>, <gregtech:machine:503>, <enderio:item_material:56>], [null, <gregtech:cable:5112>, null]]);
recipes.addShaped(<enderio:item_material:66>, [[null, <gregtech:cable:5112>, null],[<enderio:item_alloy_ingot:8>, <gregtech:machine:503>, <enderio:item_alloy_ingot:8>], [null, <gregtech:cable:5112>, null]]);
recipes.addShaped(<enderio:item_material:53>, [[null, <gregtech:cable:5112>, null],[<enderio:item_alloy_ingot:7>, <gregtech:machine:503>, <enderio:item_alloy_ingot:7>], [null, <gregtech:cable:5112>, null]]);
recipes.addShaped(<enderio:item_material:1>, [[null, <gregtech:cable:5112>, null],[<enderio:item_material:20>, <gregtech:machine:503>, <enderio:item_material:20>], [null, <gregtech:cable:5112>, null]]);
furnace.addRecipe(<gregtech:meta_item_1:10051>, <gregtech:meta_item_1:2051>, 0.0);
recipes.addShaped(<enderio:item_material:0>, [[null, null, null],[<enderio:item_material:20>, <gregtech:machine:501>, <enderio:item_material:20>], [null, null, null]]);
recipes.addShapeless(<gregtech:meta_item_1:2237>, [<ore:dustRegularCopper>, <minecraft:redstone>, <minecraft:redstone>, <minecraft:redstone>, <minecraft:redstone>]);
recipes.addShapeless(<gregtech:meta_item_2:32443>, [<minecraft:planks>,<gregtech:meta_item_1:32627>, <gregtech:meta_item_1:32627>]);
recipes.addShapeless(<minecraft:flint_and_steel>, [<minecraft:iron_ingot>, <minecraft:flint>]);
recipes.addShaped(<minecraft:bucket>, [[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>],[null, <minecraft:iron_ingot>, null], [null, null, null]]);
recipes.addShaped(<minecraft:bucket>, [[null, null, null],[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>], [null, <minecraft:iron_ingot>, null]]);
recipes.addShaped(<minecraft:iron_boots>, [[null, null, null],[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, null, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:iron_leggings>, [[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, null, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:iron_chestplate>, [[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>],[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:iron_helmet>, [[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>], [null, null, null]]);
//File End
