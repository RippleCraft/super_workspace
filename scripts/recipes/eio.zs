//组装机
val assembler = mods.gregtech.recipe.RecipeMap.getByName("assembler");
//玄钢锭
var darkS =  <enderio:item_alloy_ingot:6>;
//末影钢锭
var endS = <enderio:item_alloy_ingot:8>;

//移除所有机械框架
mods.enderio.AlloySmelter.removeRecipe(<enderio:item_material:1>);
mods.enderio.AlloySmelter.removeRecipe(<enderio:item_material:53>);
mods.enderio.AlloySmelter.removeRecipe(<enderio:item_material:54>);
//电容
val capacitor1 = <enderio:item_basic_capacitor:0>;
val capacitor2 = <enderio:item_basic_capacitor:1>;
val capacitor3 = <enderio:item_basic_capacitor:2>;

//基础电容
assembler.recipeBuilder()
    .inputs([<gregtech:meta_item_1:12026>*2,<enderio:item_material:20>*2,<gregtech:meta_item_2:16018>*16])
    .outputs(capacitor1)
    .duration(20*5)
    .EUt(480)
    .property("circuit", 1)
    .buildAndRegister();

//双层电容
assembler.recipeBuilder()
    .inputs([capacitor1*2,<enderio:item_alloy_ingot:1>*2,<gregtech:meta_item_2:16026>*16])
    .outputs(capacitor2)
    .duration(20*5)
    .EUt(480)
    .property("circuit", 1)
    .buildAndRegister();

//八位电容
assembler.recipeBuilder()
    .inputs([capacitor2*2,<enderio:item_alloy_ingot:2>*2,<gregtech:meta_item_2:16072>*16])
    .outputs(capacitor3)
    .duration(20*5)
    .EUt(480)
    .property("circuit", 1)
    .buildAndRegister();

//玄钢套
assembler.recipeBuilder()
    .inputs([darkS*5,<gregtech:meta_item_1:17183>*10,<gregtech:meta_item_2:16112>*32,<gregtech:meta_item_1:32692>*2,<gregtech:meta_item_1:32602>*2])
    .fluidInputs([<liquid:soldering_alloy> * 288])
    .outputs(<enderio:item_dark_steel_helmet>)
    .duration(20*20)
    .EUt(480)
    .property("circuit", 1)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([darkS*8,<gregtech:meta_item_1:17183>*16,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_1:32652>*4])
    .fluidInputs([<liquid:soldering_alloy> * 576])
    .outputs(<enderio:item_dark_steel_chestplate>)
    .duration(20*40)
    .EUt(480)
    .property("circuit", 2)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([darkS*7,<gregtech:meta_item_1:17183>*14,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_2:16112>*32,<gregtech:meta_item_1:32602>*4,<gregtech:meta_item_1:32642>*2])
    .fluidInputs([<liquid:soldering_alloy> * 576])
    .outputs(<enderio:item_dark_steel_leggings>)
    .duration(20*30)
    .EUt(480)
    .property("circuit", 3)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([darkS*4,<gregtech:meta_item_1:17183>*10,<gregtech:meta_item_2:16112>*32,<gregtech:meta_item_1:32602>*2])
    .fluidInputs([<liquid:soldering_alloy> * 288])
    .outputs(<enderio:item_dark_steel_boots>)
    .duration(20*20)
    .EUt(480)
    .property("circuit", 4)
    .buildAndRegister();

//末影钢套
assembler.recipeBuilder()
    .inputs([endS*5,<gregtech:meta_item_1:17072>*10,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_1:32692>*2,<gregtech:meta_item_1:32603>*2])
    .fluidInputs([<liquid:stainless_steel> * 288])
    .outputs(<enderio:item_end_steel_helmet>)
    .duration(20*40)
    .EUt(2000)
    .property("circuit", 1)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([endS*8,<gregtech:meta_item_1:17072>*16,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_1:32653>*4])
    .fluidInputs([<liquid:stainless_steel> * 576])
    .outputs(<enderio:item_end_steel_chestplate>)
    .duration(20*80)
    .EUt(2000)
    .property("circuit", 2)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([endS*7,<gregtech:meta_item_1:17072>*14,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_1:32603>*4,<gregtech:meta_item_1:32643>*2])
    .fluidInputs([<liquid:stainless_steel> * 576])
    .outputs(<enderio:item_end_steel_leggings>)
    .duration(20*60)
    .EUt(2000)
    .property("circuit", 3)
    .buildAndRegister();

assembler.recipeBuilder()
    .inputs([endS*4,<gregtech:meta_item_1:17072>*10,<gregtech:meta_item_2:16112>*64,<gregtech:meta_item_1:32603>*2])
    .fluidInputs([<liquid:stainless_steel> * 288])
    .outputs(<enderio:item_end_steel_boots>)
    .duration(20*40)
    .EUt(2000)
    .property("circuit", 4)
    .buildAndRegister();