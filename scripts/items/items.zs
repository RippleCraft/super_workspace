#loader contenttweaker

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Item;

//注魔钻石
var super_diamond = VanillaFactory.createItem("super_diamond");
super_diamond.maxStackSize = 1;
super_diamond.creativeTab = <creativetab:misc>;
super_diamond.register();

//工厂许可证
var t_license = VanillaFactory.createItem("t_license");
t_license.maxStackSize = 1;
t_license.creativeTab = <creativetab:misc>;
t_license.register();

//巫师许可证
var m_license = VanillaFactory.createItem("m_license");
m_license.maxStackSize = 1;
m_license.creativeTab = <creativetab:misc>;
m_license.register();

//庄园许可证
var a_license = VanillaFactory.createItem("a_license");
a_license.maxStackSize = 1;
a_license.creativeTab = <creativetab:misc>;
a_license.register();

//涟漪贡献铜币
var r_coin_0_0 = VanillaFactory.createItem("r_coin_0_0");
r_coin_0_0.maxStackSize = 64;
r_coin_0_0.creativeTab = <creativetab:misc>;
r_coin_0_0.register();

//涟漪贡献银币
var r_coin_0_1 = VanillaFactory.createItem("r_coin_0_1");
r_coin_0_1.maxStackSize = 64;
r_coin_0_1.creativeTab = <creativetab:misc>;
r_coin_0_1.register();

//涟漪贡献金币
var r_coin_0_2 = VanillaFactory.createItem("r_coin_0_2");
r_coin_0_2.maxStackSize = 64;
r_coin_0_2.creativeTab = <creativetab:misc>;
r_coin_0_2.register();

//涟漪锭
var ripple_ingot = VanillaFactory.createItem("ripple_ingot");
ripple_ingot.maxStackSize = 64;
ripple_ingot.creativeTab = <creativetab:misc>;
ripple_ingot.register();